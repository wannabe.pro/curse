﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Data;

namespace PBX
{
    public partial class DataSet : ISupportInitialize, IDisposable
    {
        public new void EndInit()
        {
            base.EndInit();
            this.Calls.TableNewRow += new DataTableNewRowEventHandler(this.TableNewRowHeandler);
            this.Calls.CallsRowChanged += new DataSet.CallsRowChangeEventHandler(this.CallsRowChangedHandler);
            this.Calls.CallsRowDeleted += new DataSet.CallsRowChangeEventHandler(this.CallsRowChangedHandler);
        }

        public void TableNewRowHeandler(object sender, DataTableNewRowEventArgs e)
        {
            if (DBNull.Value.Equals(e.Row["Timestamp"]))
            {
                e.Row["Timestamp"] = DateTime.Now;
            }
        }

        public new void ReadXml(Stream stream)
        {
            var tempFile = Path.GetTempFileName();
            this.WriteXml(tempFile);
            this.Clear();
            try
            {
                base.ReadXml(stream);
            }
            catch (Exception exception)
            {
                base.ReadXml(tempFile);
                MessageBox.Show(exception.Message, exception.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            File.Delete(tempFile);
        }


        public void CallsRowChangedHandler(object sender, DataSet.CallsRowChangeEvent e)
        {
            var calls = this.Calls.ToList();

            this.Reports.Clear();
            var reports = new List<ReportsRow>(
                from call in calls
                group call by call.ClientsRow
                into groups
                select this.Reports.AddReportsRow(groups.Key, groups.Sum(s => s.Minutes), groups.Sum(s => s.Minutes) * groups.Key.RatesRow.Minute, groups.Key.Name, groups.Key.RatesRow.Name, groups.Key.RatesRow.Minute)
            );

            this.Activities.Clear();
            var activities = new List<ActivitiesRow>(
                from call in calls
                group call by call.ClientsRow.RatesRow
                into groups
                select this.Activities.AddActivitiesRow(groups.Key, groups.Sum(s => s.Minutes), groups.Sum(s => s.Minutes) * groups.Key.Minute)
            );
        }

        public new void Clear()
        {
            base.Clear();

            var rates = new List<DataSet.RatesRow>()
            {
                this.Rates.AddRatesRow("Бюджетная организация", 0.4),
                this.Rates.AddRatesRow("Коммерческая организация", 0.3),
                this.Rates.AddRatesRow("Физическое лицо", 0.2),
                this.Rates.AddRatesRow("Льготник", 0.1),
            };

            var clients = new List<DataSet.ClientsRow>()
            {
                this.Clients.AddClientsRow(rates[0], "А"),
                this.Clients.AddClientsRow(rates[0], "Б"),
                this.Clients.AddClientsRow(rates[1], "В"),
                this.Clients.AddClientsRow(rates[1], "Г"),
                this.Clients.AddClientsRow(rates[2], "Д"),
                this.Clients.AddClientsRow(rates[2], "Е"),
                this.Clients.AddClientsRow(rates[3], "И"),
                this.Clients.AddClientsRow(rates[3], "Ж"),
            };

            var calls = new List<DataSet.CallsRow>()
            {
                this.Calls.AddCallsRow(clients[0], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[0], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[1], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[1], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[2], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[2], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[3], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[3], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[4], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[4], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[5], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[5], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[6], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[6], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[7], 1.0, DateTime.Parse("2018-01-01T00:00:00")),
                this.Calls.AddCallsRow(clients[7], 1.0, DateTime.Parse("2018-01-01T00:00:00"))
            };
        }

        public new void Dispose()
        {
            this.Calls.CallsRowChanged -= new DataSet.CallsRowChangeEventHandler(this.CallsRowChangedHandler);
            base.Clear();
            base.Dispose();
        }
    }
}
