﻿namespace PBX
{
    partial class Form
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.TabPage RatesTabPage;
            System.Windows.Forms.ToolStripContainer RatesToolStripContainer;
            System.Windows.Forms.DataGridView RatesDataGridView;
            System.Windows.Forms.DataGridViewTextBoxColumn RatesNameDataGridViewTextBoxColumn;
            System.Windows.Forms.DataGridViewTextBoxColumn RatesMinuteDataGridViewTextBoxColumn;
            System.Windows.Forms.BindingSource RatesBindingSource;
            System.Windows.Forms.ToolStrip RatesToolStrip;
            System.Windows.Forms.ToolStripLabel RatesToolStripLabel;
            System.Windows.Forms.ToolStripButton RatesExportToolStripButton;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form));
            System.Windows.Forms.ToolStripContainer MainToolStripContainer;
            System.Windows.Forms.StatusStrip StatusStrip;
            System.Windows.Forms.ToolStripStatusLabel StateStripStatusLabel;
            System.Windows.Forms.TabControl TabControl;
            System.Windows.Forms.TabPage ClientsTabPage;
            System.Windows.Forms.ToolStripContainer ClientsToolStripContainer;
            System.Windows.Forms.DataGridView ClientsDataGridView;
            System.Windows.Forms.DataGridViewComboBoxColumn ClientsRateDataGridViewTextBoxColumn;
            System.Windows.Forms.DataGridViewTextBoxColumn ClientsNumberDataGridViewTextBoxColumn;
            System.Windows.Forms.DataGridViewTextBoxColumn ClientsNameDataGridViewTextBoxColumn;
            System.Windows.Forms.BindingSource ClientsBindingSource;
            System.Windows.Forms.ToolStrip ClientsToolStrip;
            System.Windows.Forms.ToolStripLabel ClientsToolStripLabel;
            System.Windows.Forms.ToolStripButton ClientsExportToolStripButton;
            System.Windows.Forms.TabPage CallsTabPage;
            System.Windows.Forms.ToolStripContainer CallsToolStripContainer;
            System.Windows.Forms.DataGridView CallsDataGridView;
            System.Windows.Forms.DataGridViewComboBoxColumn CallsClientDataGridViewTextBoxColumn;
            System.Windows.Forms.DataGridViewTextBoxColumn CallsMinutesDataGridViewTextBoxColumn;
            System.Windows.Forms.DataGridViewTextBoxColumn CallsTimestampDataGridViewTextBoxColumn;
            System.Windows.Forms.BindingSource CallsBindingSource;
            System.Windows.Forms.ToolStrip CallsToolStrip;
            System.Windows.Forms.ToolStripLabel CallsToolStripLabel;
            System.Windows.Forms.ToolStripButton CallsExportToolStripButton;
            System.Windows.Forms.TabPage ReportsTabPage;
            System.Windows.Forms.ToolStripContainer ReportsToolStripContainer;
            System.Windows.Forms.DataGridView ReportsDataGridView;
            System.Windows.Forms.DataGridViewTextBoxColumn ReportsClientDataGridViewTextBoxColumn;
            System.Windows.Forms.DataGridViewTextBoxColumn ReportsClientNameDataGridViewTextBoxColumn;
            System.Windows.Forms.DataGridViewTextBoxColumn ReportsRateNameGridViewTextBoxColumn;
            System.Windows.Forms.DataGridViewTextBoxColumn ReportsRateMinuteDataGridViewTextBoxColumn;
            System.Windows.Forms.DataGridViewTextBoxColumn ReportsMinutesDataGridViewTextBoxColumn;
            System.Windows.Forms.DataGridViewTextBoxColumn ReportsBillDataGridViewTextBoxColumn;
            System.Windows.Forms.BindingSource ReportsBindingSource;
            System.Windows.Forms.ToolStrip ReportsToolStrip;
            System.Windows.Forms.ToolStripLabel ReportsToolStripLabel;
            System.Windows.Forms.ToolStripButton ReportsToolStripButton;
            System.Windows.Forms.TabPage ActivitiesTabPage;
            System.Windows.Forms.ToolStripContainer ActivitiesToolStripContainer;
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.BindingSource ActivitiesBindingSource;
            System.Windows.Forms.ToolStrip ActivitiesToolStrip;
            System.Windows.Forms.ToolStripLabel ActivitiesToolStripLabel;
            System.Windows.Forms.ToolStripButton ActivitiesExportToolStripButton;
            System.Windows.Forms.ToolStrip MainToolStrip;
            System.Windows.Forms.ToolStripLabel MainToolStripLabel;
            System.Windows.Forms.ToolStripButton MainCreateToolStripButton;
            System.Windows.Forms.ToolStripButton MainOpenStripButton;
            System.Windows.Forms.ToolStripButton MainSaveToolStripButton;
            System.Windows.Forms.ToolStripButton MainExitToolStripButton;
            this.DataSet = new PBX.DataSet();
            this.ActivitiesChart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.DataSetSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.DataSetOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.TableSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.ChartSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            RatesTabPage = new System.Windows.Forms.TabPage();
            RatesToolStripContainer = new System.Windows.Forms.ToolStripContainer();
            RatesDataGridView = new System.Windows.Forms.DataGridView();
            RatesNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            RatesMinuteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            RatesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            RatesToolStrip = new System.Windows.Forms.ToolStrip();
            RatesToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            RatesExportToolStripButton = new System.Windows.Forms.ToolStripButton();
            MainToolStripContainer = new System.Windows.Forms.ToolStripContainer();
            StatusStrip = new System.Windows.Forms.StatusStrip();
            StateStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            TabControl = new System.Windows.Forms.TabControl();
            ClientsTabPage = new System.Windows.Forms.TabPage();
            ClientsToolStripContainer = new System.Windows.Forms.ToolStripContainer();
            ClientsDataGridView = new System.Windows.Forms.DataGridView();
            ClientsRateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            ClientsNumberDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ClientsNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ClientsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ClientsToolStrip = new System.Windows.Forms.ToolStrip();
            ClientsToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            ClientsExportToolStripButton = new System.Windows.Forms.ToolStripButton();
            CallsTabPage = new System.Windows.Forms.TabPage();
            CallsToolStripContainer = new System.Windows.Forms.ToolStripContainer();
            CallsDataGridView = new System.Windows.Forms.DataGridView();
            CallsClientDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            CallsMinutesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            CallsTimestampDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            CallsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            CallsToolStrip = new System.Windows.Forms.ToolStrip();
            CallsToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            CallsExportToolStripButton = new System.Windows.Forms.ToolStripButton();
            ReportsTabPage = new System.Windows.Forms.TabPage();
            ReportsToolStripContainer = new System.Windows.Forms.ToolStripContainer();
            ReportsDataGridView = new System.Windows.Forms.DataGridView();
            ReportsClientDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ReportsClientNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ReportsRateNameGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ReportsRateMinuteDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ReportsMinutesDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ReportsBillDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ReportsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ReportsToolStrip = new System.Windows.Forms.ToolStrip();
            ReportsToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            ReportsToolStripButton = new System.Windows.Forms.ToolStripButton();
            ActivitiesTabPage = new System.Windows.Forms.TabPage();
            ActivitiesToolStripContainer = new System.Windows.Forms.ToolStripContainer();
            ActivitiesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ActivitiesToolStrip = new System.Windows.Forms.ToolStrip();
            ActivitiesToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            ActivitiesExportToolStripButton = new System.Windows.Forms.ToolStripButton();
            MainToolStrip = new System.Windows.Forms.ToolStrip();
            MainToolStripLabel = new System.Windows.Forms.ToolStripLabel();
            MainCreateToolStripButton = new System.Windows.Forms.ToolStripButton();
            MainOpenStripButton = new System.Windows.Forms.ToolStripButton();
            MainSaveToolStripButton = new System.Windows.Forms.ToolStripButton();
            MainExitToolStripButton = new System.Windows.Forms.ToolStripButton();
            RatesTabPage.SuspendLayout();
            RatesToolStripContainer.ContentPanel.SuspendLayout();
            RatesToolStripContainer.TopToolStripPanel.SuspendLayout();
            RatesToolStripContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(RatesDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(RatesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSet)).BeginInit();
            RatesToolStrip.SuspendLayout();
            MainToolStripContainer.BottomToolStripPanel.SuspendLayout();
            MainToolStripContainer.ContentPanel.SuspendLayout();
            MainToolStripContainer.TopToolStripPanel.SuspendLayout();
            MainToolStripContainer.SuspendLayout();
            StatusStrip.SuspendLayout();
            TabControl.SuspendLayout();
            ClientsTabPage.SuspendLayout();
            ClientsToolStripContainer.ContentPanel.SuspendLayout();
            ClientsToolStripContainer.TopToolStripPanel.SuspendLayout();
            ClientsToolStripContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(ClientsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(ClientsBindingSource)).BeginInit();
            ClientsToolStrip.SuspendLayout();
            CallsTabPage.SuspendLayout();
            CallsToolStripContainer.ContentPanel.SuspendLayout();
            CallsToolStripContainer.TopToolStripPanel.SuspendLayout();
            CallsToolStripContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(CallsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(CallsBindingSource)).BeginInit();
            CallsToolStrip.SuspendLayout();
            ReportsTabPage.SuspendLayout();
            ReportsToolStripContainer.ContentPanel.SuspendLayout();
            ReportsToolStripContainer.TopToolStripPanel.SuspendLayout();
            ReportsToolStripContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(ReportsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(ReportsBindingSource)).BeginInit();
            ReportsToolStrip.SuspendLayout();
            ActivitiesTabPage.SuspendLayout();
            ActivitiesToolStripContainer.ContentPanel.SuspendLayout();
            ActivitiesToolStripContainer.TopToolStripPanel.SuspendLayout();
            ActivitiesToolStripContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ActivitiesChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(ActivitiesBindingSource)).BeginInit();
            ActivitiesToolStrip.SuspendLayout();
            MainToolStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // RatesTabPage
            // 
            RatesTabPage.Controls.Add(RatesToolStripContainer);
            RatesTabPage.Location = new System.Drawing.Point(4, 4);
            RatesTabPage.Name = "RatesTabPage";
            RatesTabPage.Size = new System.Drawing.Size(792, 377);
            RatesTabPage.TabIndex = 0;
            RatesTabPage.Text = "Тарифы";
            RatesTabPage.ToolTipText = "Тарифные планы клиентов";
            RatesTabPage.UseVisualStyleBackColor = true;
            // 
            // RatesToolStripContainer
            // 
            // 
            // RatesToolStripContainer.ContentPanel
            // 
            RatesToolStripContainer.ContentPanel.Controls.Add(RatesDataGridView);
            RatesToolStripContainer.ContentPanel.Size = new System.Drawing.Size(792, 352);
            RatesToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            RatesToolStripContainer.Location = new System.Drawing.Point(0, 0);
            RatesToolStripContainer.Name = "RatesToolStripContainer";
            RatesToolStripContainer.Size = new System.Drawing.Size(792, 377);
            RatesToolStripContainer.TabIndex = 1;
            // 
            // RatesToolStripContainer.TopToolStripPanel
            // 
            RatesToolStripContainer.TopToolStripPanel.Controls.Add(RatesToolStrip);
            // 
            // RatesDataGridView
            // 
            RatesDataGridView.AutoGenerateColumns = false;
            RatesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            RatesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            RatesDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            RatesNameDataGridViewTextBoxColumn,
            RatesMinuteDataGridViewTextBoxColumn});
            RatesDataGridView.DataSource = RatesBindingSource;
            RatesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            RatesDataGridView.Location = new System.Drawing.Point(0, 0);
            RatesDataGridView.Name = "RatesDataGridView";
            RatesDataGridView.Size = new System.Drawing.Size(792, 352);
            RatesDataGridView.TabIndex = 0;
            // 
            // RatesNameDataGridViewTextBoxColumn
            // 
            RatesNameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            RatesNameDataGridViewTextBoxColumn.HeaderText = "Название";
            RatesNameDataGridViewTextBoxColumn.Name = "RatesNameDataGridViewTextBoxColumn";
            RatesNameDataGridViewTextBoxColumn.ToolTipText = "Название тарифного плана";
            RatesNameDataGridViewTextBoxColumn.Width = 82;
            // 
            // RatesMinuteDataGridViewTextBoxColumn
            // 
            RatesMinuteDataGridViewTextBoxColumn.DataPropertyName = "Minute";
            RatesMinuteDataGridViewTextBoxColumn.HeaderText = "Стоимость минуты";
            RatesMinuteDataGridViewTextBoxColumn.Name = "RatesMinuteDataGridViewTextBoxColumn";
            RatesMinuteDataGridViewTextBoxColumn.ToolTipText = "Стоить минуты по тарифу в рублях";
            RatesMinuteDataGridViewTextBoxColumn.Width = 117;
            // 
            // RatesBindingSource
            // 
            RatesBindingSource.DataMember = "Rates";
            RatesBindingSource.DataSource = this.DataSet;
            // 
            // DataSet
            // 
            this.DataSet.DataSetName = "DataSet";
            this.DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // RatesToolStrip
            // 
            RatesToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            RatesToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            RatesToolStripLabel,
            RatesExportToolStripButton});
            RatesToolStrip.Location = new System.Drawing.Point(3, 0);
            RatesToolStrip.Name = "RatesToolStrip";
            RatesToolStrip.Size = new System.Drawing.Size(167, 25);
            RatesToolStrip.TabIndex = 0;
            // 
            // RatesToolStripLabel
            // 
            RatesToolStripLabel.Enabled = false;
            RatesToolStripLabel.Name = "RatesToolStripLabel";
            RatesToolStripLabel.Size = new System.Drawing.Size(55, 22);
            RatesToolStripLabel.Text = "Тарифы:";
            // 
            // RatesExportToolStripButton
            // 
            RatesExportToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            RatesExportToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("RatesExportToolStripButton.Image")));
            RatesExportToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            RatesExportToolStripButton.Name = "RatesExportToolStripButton";
            RatesExportToolStripButton.Size = new System.Drawing.Size(100, 22);
            RatesExportToolStripButton.Text = "Экспортировать";
            RatesExportToolStripButton.ToolTipText = "Экспортировать таблицу тарифов";
            RatesExportToolStripButton.Click += new System.EventHandler(this.RatesExportToolStripButton_Click);
            // 
            // MainToolStripContainer
            // 
            // 
            // MainToolStripContainer.BottomToolStripPanel
            // 
            MainToolStripContainer.BottomToolStripPanel.Controls.Add(StatusStrip);
            // 
            // MainToolStripContainer.ContentPanel
            // 
            MainToolStripContainer.ContentPanel.Controls.Add(TabControl);
            MainToolStripContainer.ContentPanel.Size = new System.Drawing.Size(800, 403);
            MainToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            MainToolStripContainer.Location = new System.Drawing.Point(0, 0);
            MainToolStripContainer.Name = "MainToolStripContainer";
            MainToolStripContainer.Size = new System.Drawing.Size(800, 450);
            MainToolStripContainer.TabIndex = 1;
            // 
            // MainToolStripContainer.TopToolStripPanel
            // 
            MainToolStripContainer.TopToolStripPanel.Controls.Add(MainToolStrip);
            // 
            // StatusStrip
            // 
            StatusStrip.Dock = System.Windows.Forms.DockStyle.None;
            StatusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            StateStripStatusLabel});
            StatusStrip.Location = new System.Drawing.Point(0, 0);
            StatusStrip.Name = "StatusStrip";
            StatusStrip.Size = new System.Drawing.Size(800, 22);
            StatusStrip.TabIndex = 1;
            // 
            // StateStripStatusLabel
            // 
            StateStripStatusLabel.Enabled = false;
            StateStripStatusLabel.Name = "StateStripStatusLabel";
            StateStripStatusLabel.Size = new System.Drawing.Size(69, 17);
            StateStripStatusLabel.Text = "Состояние:";
            // 
            // TabControl
            // 
            TabControl.Alignment = System.Windows.Forms.TabAlignment.Bottom;
            TabControl.Controls.Add(RatesTabPage);
            TabControl.Controls.Add(ClientsTabPage);
            TabControl.Controls.Add(CallsTabPage);
            TabControl.Controls.Add(ReportsTabPage);
            TabControl.Controls.Add(ActivitiesTabPage);
            TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            TabControl.Location = new System.Drawing.Point(0, 0);
            TabControl.Multiline = true;
            TabControl.Name = "TabControl";
            TabControl.SelectedIndex = 0;
            TabControl.Size = new System.Drawing.Size(800, 403);
            TabControl.TabIndex = 0;
            // 
            // ClientsTabPage
            // 
            ClientsTabPage.Controls.Add(ClientsToolStripContainer);
            ClientsTabPage.Location = new System.Drawing.Point(4, 4);
            ClientsTabPage.Name = "ClientsTabPage";
            ClientsTabPage.Size = new System.Drawing.Size(792, 377);
            ClientsTabPage.TabIndex = 1;
            ClientsTabPage.Text = "Клиенты";
            ClientsTabPage.ToolTipText = "Список клиентов";
            ClientsTabPage.UseVisualStyleBackColor = true;
            // 
            // ClientsToolStripContainer
            // 
            // 
            // ClientsToolStripContainer.ContentPanel
            // 
            ClientsToolStripContainer.ContentPanel.Controls.Add(ClientsDataGridView);
            ClientsToolStripContainer.ContentPanel.Size = new System.Drawing.Size(792, 352);
            ClientsToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            ClientsToolStripContainer.Location = new System.Drawing.Point(0, 0);
            ClientsToolStripContainer.Name = "ClientsToolStripContainer";
            ClientsToolStripContainer.Size = new System.Drawing.Size(792, 377);
            ClientsToolStripContainer.TabIndex = 1;
            ClientsToolStripContainer.Text = "toolStripContainer1";
            // 
            // ClientsToolStripContainer.TopToolStripPanel
            // 
            ClientsToolStripContainer.TopToolStripPanel.Controls.Add(ClientsToolStrip);
            // 
            // ClientsDataGridView
            // 
            ClientsDataGridView.AutoGenerateColumns = false;
            ClientsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            ClientsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            ClientsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            ClientsRateDataGridViewTextBoxColumn,
            ClientsNumberDataGridViewTextBoxColumn,
            ClientsNameDataGridViewTextBoxColumn});
            ClientsDataGridView.DataSource = ClientsBindingSource;
            ClientsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            ClientsDataGridView.Location = new System.Drawing.Point(0, 0);
            ClientsDataGridView.Name = "ClientsDataGridView";
            ClientsDataGridView.Size = new System.Drawing.Size(792, 352);
            ClientsDataGridView.TabIndex = 0;
            // 
            // ClientsRateDataGridViewTextBoxColumn
            // 
            ClientsRateDataGridViewTextBoxColumn.DataPropertyName = "Rate";
            ClientsRateDataGridViewTextBoxColumn.DataSource = RatesBindingSource;
            ClientsRateDataGridViewTextBoxColumn.DisplayMember = "Name";
            ClientsRateDataGridViewTextBoxColumn.HeaderText = "Тариф";
            ClientsRateDataGridViewTextBoxColumn.Name = "ClientsRateDataGridViewTextBoxColumn";
            ClientsRateDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            ClientsRateDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            ClientsRateDataGridViewTextBoxColumn.ToolTipText = "Тариф клиента";
            ClientsRateDataGridViewTextBoxColumn.ValueMember = "Name";
            ClientsRateDataGridViewTextBoxColumn.Width = 65;
            // 
            // ClientsNumberDataGridViewTextBoxColumn
            // 
            ClientsNumberDataGridViewTextBoxColumn.DataPropertyName = "Number";
            ClientsNumberDataGridViewTextBoxColumn.HeaderText = "Номер";
            ClientsNumberDataGridViewTextBoxColumn.Name = "ClientsNumberDataGridViewTextBoxColumn";
            ClientsNumberDataGridViewTextBoxColumn.ToolTipText = "Адресный номер клиента";
            ClientsNumberDataGridViewTextBoxColumn.Width = 66;
            // 
            // ClientsNameDataGridViewTextBoxColumn
            // 
            ClientsNameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            ClientsNameDataGridViewTextBoxColumn.HeaderText = "ФИО";
            ClientsNameDataGridViewTextBoxColumn.Name = "ClientsNameDataGridViewTextBoxColumn";
            ClientsNameDataGridViewTextBoxColumn.ToolTipText = "ФИО клиента";
            ClientsNameDataGridViewTextBoxColumn.Width = 59;
            // 
            // ClientsBindingSource
            // 
            ClientsBindingSource.DataMember = "Clients";
            ClientsBindingSource.DataSource = this.DataSet;
            // 
            // ClientsToolStrip
            // 
            ClientsToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            ClientsToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            ClientsToolStripLabel,
            ClientsExportToolStripButton});
            ClientsToolStrip.Location = new System.Drawing.Point(3, 0);
            ClientsToolStrip.Name = "ClientsToolStrip";
            ClientsToolStrip.Size = new System.Drawing.Size(170, 25);
            ClientsToolStrip.TabIndex = 0;
            // 
            // ClientsToolStripLabel
            // 
            ClientsToolStripLabel.Enabled = false;
            ClientsToolStripLabel.Name = "ClientsToolStripLabel";
            ClientsToolStripLabel.Size = new System.Drawing.Size(58, 22);
            ClientsToolStripLabel.Text = "Клиенты:";
            // 
            // ClientsExportToolStripButton
            // 
            ClientsExportToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            ClientsExportToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("ClientsExportToolStripButton.Image")));
            ClientsExportToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            ClientsExportToolStripButton.Name = "ClientsExportToolStripButton";
            ClientsExportToolStripButton.Size = new System.Drawing.Size(100, 22);
            ClientsExportToolStripButton.Text = "Экспортировать";
            ClientsExportToolStripButton.ToolTipText = "Экспортировать таблицу клиентов";
            ClientsExportToolStripButton.Click += new System.EventHandler(this.ClientsExportToolStripButton_Click);
            // 
            // CallsTabPage
            // 
            CallsTabPage.Controls.Add(CallsToolStripContainer);
            CallsTabPage.Location = new System.Drawing.Point(4, 4);
            CallsTabPage.Name = "CallsTabPage";
            CallsTabPage.Size = new System.Drawing.Size(792, 377);
            CallsTabPage.TabIndex = 2;
            CallsTabPage.Text = "Звонки";
            CallsTabPage.ToolTipText = "Лог звонков";
            CallsTabPage.UseVisualStyleBackColor = true;
            // 
            // CallsToolStripContainer
            // 
            // 
            // CallsToolStripContainer.ContentPanel
            // 
            CallsToolStripContainer.ContentPanel.Controls.Add(CallsDataGridView);
            CallsToolStripContainer.ContentPanel.Size = new System.Drawing.Size(792, 352);
            CallsToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            CallsToolStripContainer.Location = new System.Drawing.Point(0, 0);
            CallsToolStripContainer.Name = "CallsToolStripContainer";
            CallsToolStripContainer.Size = new System.Drawing.Size(792, 377);
            CallsToolStripContainer.TabIndex = 1;
            CallsToolStripContainer.Text = "toolStripContainer1";
            // 
            // CallsToolStripContainer.TopToolStripPanel
            // 
            CallsToolStripContainer.TopToolStripPanel.Controls.Add(CallsToolStrip);
            // 
            // CallsDataGridView
            // 
            CallsDataGridView.AutoGenerateColumns = false;
            CallsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            CallsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            CallsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            CallsClientDataGridViewTextBoxColumn,
            CallsMinutesDataGridViewTextBoxColumn,
            CallsTimestampDataGridViewTextBoxColumn});
            CallsDataGridView.DataSource = CallsBindingSource;
            CallsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            CallsDataGridView.Location = new System.Drawing.Point(0, 0);
            CallsDataGridView.Name = "CallsDataGridView";
            CallsDataGridView.Size = new System.Drawing.Size(792, 352);
            CallsDataGridView.TabIndex = 0;
            // 
            // CallsClientDataGridViewTextBoxColumn
            // 
            CallsClientDataGridViewTextBoxColumn.DataPropertyName = "Client";
            CallsClientDataGridViewTextBoxColumn.DataSource = ClientsBindingSource;
            CallsClientDataGridViewTextBoxColumn.DisplayMember = "Number";
            CallsClientDataGridViewTextBoxColumn.HeaderText = "Клиент";
            CallsClientDataGridViewTextBoxColumn.Name = "CallsClientDataGridViewTextBoxColumn";
            CallsClientDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            CallsClientDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            CallsClientDataGridViewTextBoxColumn.ToolTipText = "Клиент совершающий звонок";
            CallsClientDataGridViewTextBoxColumn.ValueMember = "Number";
            CallsClientDataGridViewTextBoxColumn.Width = 68;
            // 
            // CallsMinutesDataGridViewTextBoxColumn
            // 
            CallsMinutesDataGridViewTextBoxColumn.DataPropertyName = "Minutes";
            CallsMinutesDataGridViewTextBoxColumn.HeaderText = "Продолжительность";
            CallsMinutesDataGridViewTextBoxColumn.Name = "CallsMinutesDataGridViewTextBoxColumn";
            CallsMinutesDataGridViewTextBoxColumn.ToolTipText = "Продолжительность звонка в минутах";
            CallsMinutesDataGridViewTextBoxColumn.Width = 136;
            // 
            // CallsTimestampDataGridViewTextBoxColumn
            // 
            CallsTimestampDataGridViewTextBoxColumn.DataPropertyName = "Timestamp";
            CallsTimestampDataGridViewTextBoxColumn.HeaderText = "Начало";
            CallsTimestampDataGridViewTextBoxColumn.Name = "CallsTimestampDataGridViewTextBoxColumn";
            CallsTimestampDataGridViewTextBoxColumn.ToolTipText = "Начало тарификации звонка";
            CallsTimestampDataGridViewTextBoxColumn.Width = 69;
            // 
            // CallsBindingSource
            // 
            CallsBindingSource.DataMember = "Calls";
            CallsBindingSource.DataSource = this.DataSet;
            // 
            // CallsToolStrip
            // 
            CallsToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            CallsToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            CallsToolStripLabel,
            CallsExportToolStripButton});
            CallsToolStrip.Location = new System.Drawing.Point(3, 0);
            CallsToolStrip.Name = "CallsToolStrip";
            CallsToolStrip.Size = new System.Drawing.Size(162, 25);
            CallsToolStrip.TabIndex = 0;
            // 
            // CallsToolStripLabel
            // 
            CallsToolStripLabel.Enabled = false;
            CallsToolStripLabel.Name = "CallsToolStripLabel";
            CallsToolStripLabel.Size = new System.Drawing.Size(50, 22);
            CallsToolStripLabel.Text = "Звонки:";
            // 
            // CallsExportToolStripButton
            // 
            CallsExportToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            CallsExportToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("CallsExportToolStripButton.Image")));
            CallsExportToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            CallsExportToolStripButton.Name = "CallsExportToolStripButton";
            CallsExportToolStripButton.Size = new System.Drawing.Size(100, 22);
            CallsExportToolStripButton.Text = "Экспортировать";
            CallsExportToolStripButton.ToolTipText = "Экспортировать таблицу звонков";
            CallsExportToolStripButton.Click += new System.EventHandler(this.CallsExportToolStripButton_Click);
            // 
            // ReportsTabPage
            // 
            ReportsTabPage.Controls.Add(ReportsToolStripContainer);
            ReportsTabPage.Cursor = System.Windows.Forms.Cursors.Default;
            ReportsTabPage.Location = new System.Drawing.Point(4, 4);
            ReportsTabPage.Name = "ReportsTabPage";
            ReportsTabPage.Size = new System.Drawing.Size(792, 377);
            ReportsTabPage.TabIndex = 3;
            ReportsTabPage.Text = "Отчет";
            ReportsTabPage.ToolTipText = "Отчет по клиентам";
            ReportsTabPage.UseVisualStyleBackColor = true;
            // 
            // ReportsToolStripContainer
            // 
            // 
            // ReportsToolStripContainer.ContentPanel
            // 
            ReportsToolStripContainer.ContentPanel.Controls.Add(ReportsDataGridView);
            ReportsToolStripContainer.ContentPanel.Size = new System.Drawing.Size(792, 352);
            ReportsToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            ReportsToolStripContainer.Location = new System.Drawing.Point(0, 0);
            ReportsToolStripContainer.Name = "ReportsToolStripContainer";
            ReportsToolStripContainer.Size = new System.Drawing.Size(792, 377);
            ReportsToolStripContainer.TabIndex = 1;
            ReportsToolStripContainer.Text = "toolStripContainer1";
            // 
            // ReportsToolStripContainer.TopToolStripPanel
            // 
            ReportsToolStripContainer.TopToolStripPanel.Controls.Add(ReportsToolStrip);
            // 
            // ReportsDataGridView
            // 
            ReportsDataGridView.AllowUserToAddRows = false;
            ReportsDataGridView.AllowUserToDeleteRows = false;
            ReportsDataGridView.AutoGenerateColumns = false;
            ReportsDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            ReportsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            ReportsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            ReportsClientDataGridViewTextBoxColumn,
            ReportsClientNameDataGridViewTextBoxColumn,
            ReportsRateNameGridViewTextBoxColumn,
            ReportsRateMinuteDataGridViewTextBoxColumn,
            ReportsMinutesDataGridViewTextBoxColumn,
            ReportsBillDataGridViewTextBoxColumn});
            ReportsDataGridView.DataSource = ReportsBindingSource;
            ReportsDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            ReportsDataGridView.Location = new System.Drawing.Point(0, 0);
            ReportsDataGridView.Name = "ReportsDataGridView";
            ReportsDataGridView.ReadOnly = true;
            ReportsDataGridView.Size = new System.Drawing.Size(792, 352);
            ReportsDataGridView.TabIndex = 0;
            // 
            // ReportsClientDataGridViewTextBoxColumn
            // 
            ReportsClientDataGridViewTextBoxColumn.DataPropertyName = "Client";
            ReportsClientDataGridViewTextBoxColumn.HeaderText = "Клиент";
            ReportsClientDataGridViewTextBoxColumn.Name = "ReportsClientDataGridViewTextBoxColumn";
            ReportsClientDataGridViewTextBoxColumn.ReadOnly = true;
            ReportsClientDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            ReportsClientDataGridViewTextBoxColumn.ToolTipText = "Адресный номер клиента";
            ReportsClientDataGridViewTextBoxColumn.Width = 68;
            // 
            // ReportsClientNameDataGridViewTextBoxColumn
            // 
            ReportsClientNameDataGridViewTextBoxColumn.DataPropertyName = "ClientName";
            ReportsClientNameDataGridViewTextBoxColumn.HeaderText = "ФИО";
            ReportsClientNameDataGridViewTextBoxColumn.Name = "ReportsClientNameDataGridViewTextBoxColumn";
            ReportsClientNameDataGridViewTextBoxColumn.ReadOnly = true;
            ReportsClientNameDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            ReportsClientNameDataGridViewTextBoxColumn.ToolTipText = "ФИО клиента";
            ReportsClientNameDataGridViewTextBoxColumn.Width = 59;
            // 
            // ReportsRateNameGridViewTextBoxColumn
            // 
            ReportsRateNameGridViewTextBoxColumn.DataPropertyName = "RateName";
            ReportsRateNameGridViewTextBoxColumn.HeaderText = "Тариф";
            ReportsRateNameGridViewTextBoxColumn.Name = "ReportsRateNameGridViewTextBoxColumn";
            ReportsRateNameGridViewTextBoxColumn.ReadOnly = true;
            ReportsRateNameGridViewTextBoxColumn.ToolTipText = "Тарифный план клиента";
            ReportsRateNameGridViewTextBoxColumn.Width = 65;
            // 
            // ReportsRateMinuteDataGridViewTextBoxColumn
            // 
            ReportsRateMinuteDataGridViewTextBoxColumn.DataPropertyName = "RateMinute";
            ReportsRateMinuteDataGridViewTextBoxColumn.HeaderText = "Стоимость минуты";
            ReportsRateMinuteDataGridViewTextBoxColumn.Name = "ReportsRateMinuteDataGridViewTextBoxColumn";
            ReportsRateMinuteDataGridViewTextBoxColumn.ReadOnly = true;
            ReportsRateMinuteDataGridViewTextBoxColumn.ToolTipText = "Стоимость минуты по тарифному плану в рублях";
            ReportsRateMinuteDataGridViewTextBoxColumn.Width = 117;
            // 
            // ReportsMinutesDataGridViewTextBoxColumn
            // 
            ReportsMinutesDataGridViewTextBoxColumn.DataPropertyName = "Minutes";
            ReportsMinutesDataGridViewTextBoxColumn.HeaderText = "Продолжительность звонков";
            ReportsMinutesDataGridViewTextBoxColumn.Name = "ReportsMinutesDataGridViewTextBoxColumn";
            ReportsMinutesDataGridViewTextBoxColumn.ReadOnly = true;
            ReportsMinutesDataGridViewTextBoxColumn.ToolTipText = "Сумарная продолжительность звонков совершенных клиентом";
            ReportsMinutesDataGridViewTextBoxColumn.Width = 165;
            // 
            // ReportsBillDataGridViewTextBoxColumn
            // 
            ReportsBillDataGridViewTextBoxColumn.DataPropertyName = "Bill";
            ReportsBillDataGridViewTextBoxColumn.HeaderText = "Стоимость итого";
            ReportsBillDataGridViewTextBoxColumn.Name = "ReportsBillDataGridViewTextBoxColumn";
            ReportsBillDataGridViewTextBoxColumn.ReadOnly = true;
            ReportsBillDataGridViewTextBoxColumn.ToolTipText = "Итоговая стоимость совершенных звонков согластно тарифному плану клиента";
            ReportsBillDataGridViewTextBoxColumn.Width = 108;
            // 
            // ReportsBindingSource
            // 
            ReportsBindingSource.DataMember = "Reports";
            ReportsBindingSource.DataSource = this.DataSet;
            // 
            // ReportsToolStrip
            // 
            ReportsToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            ReportsToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            ReportsToolStripLabel,
            ReportsToolStripButton});
            ReportsToolStrip.Location = new System.Drawing.Point(3, 0);
            ReportsToolStrip.Name = "ReportsToolStrip";
            ReportsToolStrip.Size = new System.Drawing.Size(154, 25);
            ReportsToolStrip.TabIndex = 0;
            // 
            // ReportsToolStripLabel
            // 
            ReportsToolStripLabel.Enabled = false;
            ReportsToolStripLabel.Name = "ReportsToolStripLabel";
            ReportsToolStripLabel.Size = new System.Drawing.Size(42, 22);
            ReportsToolStripLabel.Text = "Отчет:";
            // 
            // ReportsToolStripButton
            // 
            ReportsToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            ReportsToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("ReportsToolStripButton.Image")));
            ReportsToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            ReportsToolStripButton.Name = "ReportsToolStripButton";
            ReportsToolStripButton.Size = new System.Drawing.Size(100, 22);
            ReportsToolStripButton.Text = "Экспортировать";
            ReportsToolStripButton.ToolTipText = "Экспортировать таблицу отчетов";
            ReportsToolStripButton.Click += new System.EventHandler(this.ReportsToolStripButton_Click);
            // 
            // ActivitiesTabPage
            // 
            ActivitiesTabPage.Controls.Add(ActivitiesToolStripContainer);
            ActivitiesTabPage.Location = new System.Drawing.Point(4, 4);
            ActivitiesTabPage.Name = "ActivitiesTabPage";
            ActivitiesTabPage.Size = new System.Drawing.Size(792, 377);
            ActivitiesTabPage.TabIndex = 4;
            ActivitiesTabPage.Text = "Активность";
            ActivitiesTabPage.ToolTipText = "Отчет по активности";
            ActivitiesTabPage.UseVisualStyleBackColor = true;
            // 
            // ActivitiesToolStripContainer
            // 
            // 
            // ActivitiesToolStripContainer.ContentPanel
            // 
            ActivitiesToolStripContainer.ContentPanel.Controls.Add(this.ActivitiesChart);
            ActivitiesToolStripContainer.ContentPanel.Size = new System.Drawing.Size(792, 352);
            ActivitiesToolStripContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            ActivitiesToolStripContainer.Location = new System.Drawing.Point(0, 0);
            ActivitiesToolStripContainer.Name = "ActivitiesToolStripContainer";
            ActivitiesToolStripContainer.Size = new System.Drawing.Size(792, 377);
            ActivitiesToolStripContainer.TabIndex = 1;
            ActivitiesToolStripContainer.Text = "toolStripContainer1";
            // 
            // ActivitiesToolStripContainer.TopToolStripPanel
            // 
            ActivitiesToolStripContainer.TopToolStripPanel.Controls.Add(ActivitiesToolStrip);
            // 
            // ActivitiesChart
            // 
            chartArea1.Name = "ChartArea";
            this.ActivitiesChart.ChartAreas.Add(chartArea1);
            this.ActivitiesChart.DataSource = ActivitiesBindingSource;
            this.ActivitiesChart.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend";
            this.ActivitiesChart.Legends.Add(legend1);
            this.ActivitiesChart.Location = new System.Drawing.Point(0, 0);
            this.ActivitiesChart.Name = "ActivitiesChart";
            series1.ChartArea = "ChartArea";
            series1.Legend = "Legend";
            series1.LegendText = "Продолжительность звонков";
            series1.Name = "MinutesSeries";
            series1.XValueMember = "Rate";
            series1.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series1.YValueMembers = "Minutes";
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series2.ChartArea = "ChartArea";
            series2.Legend = "Legend";
            series2.LegendText = "Стоимьсть звонков";
            series2.Name = "BillSeries";
            series2.XValueMember = "Rate";
            series2.YValueMembers = "Bill";
            this.ActivitiesChart.Series.Add(series1);
            this.ActivitiesChart.Series.Add(series2);
            this.ActivitiesChart.Size = new System.Drawing.Size(792, 352);
            this.ActivitiesChart.TabIndex = 0;
            this.ActivitiesChart.Tag = "";
            // 
            // ActivitiesBindingSource
            // 
            ActivitiesBindingSource.DataMember = "Activities";
            ActivitiesBindingSource.DataSource = this.DataSet;
            // 
            // ActivitiesToolStrip
            // 
            ActivitiesToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            ActivitiesToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            ActivitiesToolStripLabel,
            ActivitiesExportToolStripButton});
            ActivitiesToolStrip.Location = new System.Drawing.Point(3, 0);
            ActivitiesToolStrip.Name = "ActivitiesToolStrip";
            ActivitiesToolStrip.Size = new System.Drawing.Size(185, 25);
            ActivitiesToolStrip.TabIndex = 0;
            // 
            // ActivitiesToolStripLabel
            // 
            ActivitiesToolStripLabel.Enabled = false;
            ActivitiesToolStripLabel.Name = "ActivitiesToolStripLabel";
            ActivitiesToolStripLabel.Size = new System.Drawing.Size(73, 22);
            ActivitiesToolStripLabel.Text = "Активность:";
            // 
            // ActivitiesExportToolStripButton
            // 
            ActivitiesExportToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            ActivitiesExportToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("ActivitiesExportToolStripButton.Image")));
            ActivitiesExportToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            ActivitiesExportToolStripButton.Name = "ActivitiesExportToolStripButton";
            ActivitiesExportToolStripButton.Size = new System.Drawing.Size(100, 22);
            ActivitiesExportToolStripButton.Text = "Экспортировать";
            ActivitiesExportToolStripButton.ToolTipText = "Экспортировать график активности";
            ActivitiesExportToolStripButton.Click += new System.EventHandler(this.ActivitiesExportToolStripButton_Click);
            // 
            // MainToolStrip
            // 
            MainToolStrip.Dock = System.Windows.Forms.DockStyle.None;
            MainToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            MainToolStripLabel,
            MainCreateToolStripButton,
            MainOpenStripButton,
            MainSaveToolStripButton,
            MainExitToolStripButton});
            MainToolStrip.Location = new System.Drawing.Point(3, 0);
            MainToolStrip.Name = "MainToolStrip";
            MainToolStrip.Size = new System.Drawing.Size(277, 25);
            MainToolStrip.TabIndex = 0;
            // 
            // MainToolStripLabel
            // 
            MainToolStripLabel.Enabled = false;
            MainToolStripLabel.Font = new System.Drawing.Font("Segoe UI", 9F);
            MainToolStripLabel.Name = "MainToolStripLabel";
            MainToolStripLabel.Size = new System.Drawing.Size(39, 22);
            MainToolStripLabel.Text = "Файл:";
            // 
            // MainCreateToolStripButton
            // 
            MainCreateToolStripButton.BackColor = System.Drawing.SystemColors.Control;
            MainCreateToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            MainCreateToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("MainCreateToolStripButton.Image")));
            MainCreateToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            MainCreateToolStripButton.Name = "MainCreateToolStripButton";
            MainCreateToolStripButton.Size = new System.Drawing.Size(54, 22);
            MainCreateToolStripButton.Text = "Создать";
            MainCreateToolStripButton.ToolTipText = "Создать файл";
            MainCreateToolStripButton.Click += new System.EventHandler(this.MainCreateToolStripButton_Click);
            // 
            // MainOpenStripButton
            // 
            MainOpenStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            MainOpenStripButton.Image = ((System.Drawing.Image)(resources.GetObject("MainOpenStripButton.Image")));
            MainOpenStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            MainOpenStripButton.Name = "MainOpenStripButton";
            MainOpenStripButton.Size = new System.Drawing.Size(58, 22);
            MainOpenStripButton.Text = "Открыть";
            MainOpenStripButton.ToolTipText = "Открыть файл";
            MainOpenStripButton.Click += new System.EventHandler(this.MainOpenStripButton_Click);
            // 
            // MainSaveToolStripButton
            // 
            MainSaveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            MainSaveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("MainSaveToolStripButton.Image")));
            MainSaveToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            MainSaveToolStripButton.Name = "MainSaveToolStripButton";
            MainSaveToolStripButton.Size = new System.Drawing.Size(69, 22);
            MainSaveToolStripButton.Text = "Сохранить";
            MainSaveToolStripButton.ToolTipText = "Сохранить файл";
            MainSaveToolStripButton.Click += new System.EventHandler(this.MainSaveToolStripButton_Click);
            // 
            // MainExitToolStripButton
            // 
            MainExitToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            MainExitToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("MainExitToolStripButton.Image")));
            MainExitToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            MainExitToolStripButton.Name = "MainExitToolStripButton";
            MainExitToolStripButton.Size = new System.Drawing.Size(45, 22);
            MainExitToolStripButton.Text = "Выход";
            MainExitToolStripButton.ToolTipText = "Выход из приложения";
            MainExitToolStripButton.Click += new System.EventHandler(this.MainExitToolStripButton_Click);
            // 
            // DataSetSaveFileDialog
            // 
            this.DataSetSaveFileDialog.DefaultExt = "xml";
            this.DataSetSaveFileDialog.FileName = "АТС";
            this.DataSetSaveFileDialog.Filter = "Коллекция данных|*.xml|Все файлы|*.*";
            this.DataSetSaveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.DataSetSaveFileDialog_FileOk);
            // 
            // DataSetOpenFileDialog
            // 
            this.DataSetOpenFileDialog.DefaultExt = "xml";
            this.DataSetOpenFileDialog.FileName = "openFileDialog1";
            this.DataSetOpenFileDialog.Filter = "Коллекция данных|*.xml|Все файлы|*.*";
            this.DataSetOpenFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.DataSetOpenFileDialog_FileOk);
            // 
            // TableSaveFileDialog
            // 
            this.TableSaveFileDialog.DefaultExt = "xls";
            this.TableSaveFileDialog.FileName = "Новая книга";
            this.TableSaveFileDialog.Filter = "Файл Exel|*.xls|Все файлы|*.*";
            this.TableSaveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.TableSaveFileDialog_FileOk);
            // 
            // ChartSaveFileDialog
            // 
            this.ChartSaveFileDialog.DefaultExt = "png";
            this.ChartSaveFileDialog.FileName = "График";
            this.ChartSaveFileDialog.Filter = "Изображения PNG|*.png|Все файлы|*.*";
            this.ChartSaveFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(this.ChartSaveFileDialog_FileOk);
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(MainToolStripContainer);
            this.Name = "Form";
            this.Text = "АТС";
            this.Shown += new System.EventHandler(this.Form_Shown);
            RatesTabPage.ResumeLayout(false);
            RatesToolStripContainer.ContentPanel.ResumeLayout(false);
            RatesToolStripContainer.TopToolStripPanel.ResumeLayout(false);
            RatesToolStripContainer.TopToolStripPanel.PerformLayout();
            RatesToolStripContainer.ResumeLayout(false);
            RatesToolStripContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(RatesDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(RatesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataSet)).EndInit();
            RatesToolStrip.ResumeLayout(false);
            RatesToolStrip.PerformLayout();
            MainToolStripContainer.BottomToolStripPanel.ResumeLayout(false);
            MainToolStripContainer.BottomToolStripPanel.PerformLayout();
            MainToolStripContainer.ContentPanel.ResumeLayout(false);
            MainToolStripContainer.TopToolStripPanel.ResumeLayout(false);
            MainToolStripContainer.TopToolStripPanel.PerformLayout();
            MainToolStripContainer.ResumeLayout(false);
            MainToolStripContainer.PerformLayout();
            StatusStrip.ResumeLayout(false);
            StatusStrip.PerformLayout();
            TabControl.ResumeLayout(false);
            ClientsTabPage.ResumeLayout(false);
            ClientsToolStripContainer.ContentPanel.ResumeLayout(false);
            ClientsToolStripContainer.TopToolStripPanel.ResumeLayout(false);
            ClientsToolStripContainer.TopToolStripPanel.PerformLayout();
            ClientsToolStripContainer.ResumeLayout(false);
            ClientsToolStripContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(ClientsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(ClientsBindingSource)).EndInit();
            ClientsToolStrip.ResumeLayout(false);
            ClientsToolStrip.PerformLayout();
            CallsTabPage.ResumeLayout(false);
            CallsToolStripContainer.ContentPanel.ResumeLayout(false);
            CallsToolStripContainer.TopToolStripPanel.ResumeLayout(false);
            CallsToolStripContainer.TopToolStripPanel.PerformLayout();
            CallsToolStripContainer.ResumeLayout(false);
            CallsToolStripContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(CallsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(CallsBindingSource)).EndInit();
            CallsToolStrip.ResumeLayout(false);
            CallsToolStrip.PerformLayout();
            ReportsTabPage.ResumeLayout(false);
            ReportsToolStripContainer.ContentPanel.ResumeLayout(false);
            ReportsToolStripContainer.TopToolStripPanel.ResumeLayout(false);
            ReportsToolStripContainer.TopToolStripPanel.PerformLayout();
            ReportsToolStripContainer.ResumeLayout(false);
            ReportsToolStripContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(ReportsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(ReportsBindingSource)).EndInit();
            ReportsToolStrip.ResumeLayout(false);
            ReportsToolStrip.PerformLayout();
            ActivitiesTabPage.ResumeLayout(false);
            ActivitiesToolStripContainer.ContentPanel.ResumeLayout(false);
            ActivitiesToolStripContainer.TopToolStripPanel.ResumeLayout(false);
            ActivitiesToolStripContainer.TopToolStripPanel.PerformLayout();
            ActivitiesToolStripContainer.ResumeLayout(false);
            ActivitiesToolStripContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ActivitiesChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(ActivitiesBindingSource)).EndInit();
            ActivitiesToolStrip.ResumeLayout(false);
            ActivitiesToolStrip.PerformLayout();
            MainToolStrip.ResumeLayout(false);
            MainToolStrip.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private DataSet DataSet;
        private System.Windows.Forms.SaveFileDialog DataSetSaveFileDialog;
        private System.Windows.Forms.OpenFileDialog DataSetOpenFileDialog;
        private System.Windows.Forms.SaveFileDialog TableSaveFileDialog;
        private System.Windows.Forms.SaveFileDialog ChartSaveFileDialog;
        private System.Windows.Forms.DataVisualization.Charting.Chart ActivitiesChart;
    }
}

