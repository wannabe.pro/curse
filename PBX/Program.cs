﻿using System;
using System.Data;
using System.Windows.Forms;
using GemBox.Spreadsheet;

namespace PBX
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
            Application.Run(new Form());
        }

        public static void ExportToExel(string file, DataTable dt, string text)
        {
            ExcelFile ef = new ExcelFile();
            ExcelWorksheet ws = ef.Worksheets.Add(text);

            ws.InsertDataTable(
                dt,
                new InsertDataTableOptions()
                {
                    ColumnHeaders = true
                }
            );

            ef.Save(file, new XlsSaveOptions());
        }
    }
}
