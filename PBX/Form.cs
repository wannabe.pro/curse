﻿using System;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms.DataVisualization.Charting;

namespace PBX
{
    public partial class Form : System.Windows.Forms.Form
    {
        protected DataTable DataTable;
        protected string DataTableTitle;

        public Form()
        {
            InitializeComponent();
            this.DataSet.Activities.ActivitiesRowChanged += new DataSet.ActivitiesRowChangeEventHandler(this.ActivitiesRowChangedHeandler);
        }

        private void DataSetSaveFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            this.DataSet.WriteXml(this.DataSetSaveFileDialog.OpenFile());
        }

        private void DataSetOpenFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            this.DataSet.ReadXml(this.DataSetOpenFileDialog.OpenFile());
        }

        private void Form_Shown(object sender, EventArgs e)
        {
            this.DataSet.Clear();
        }

        private void MainExitToolStripButton_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void MainSaveToolStripButton_Click(object sender, EventArgs e)
        {
            this.DataSetSaveFileDialog.ShowDialog();
        }

        private void MainCreateToolStripButton_Click(object sender, EventArgs e)
        {
            this.DataSet.Clear();
        }

        private void MainOpenStripButton_Click(object sender, EventArgs e)
        {
            this.DataSetOpenFileDialog.ShowDialog();
        }

        private void RatesExportToolStripButton_Click(object sender, EventArgs e)
        {
            this.DataTable = this.DataSet.Rates;
            this.DataTableTitle = "Тарифы";
            this.TableSaveFileDialog.ShowDialog();
        }

        private void ClientsExportToolStripButton_Click(object sender, EventArgs e)
        {
            this.DataTable = this.DataSet.Clients;
            this.DataTableTitle = "Клиенты";
            this.TableSaveFileDialog.ShowDialog();
        }

        private void CallsExportToolStripButton_Click(object sender, EventArgs e)
        {
            this.DataTable = this.DataSet.Calls;
            this.DataTableTitle = "Звонки";
            this.TableSaveFileDialog.ShowDialog();
        }

        private void ReportsToolStripButton_Click(object sender, EventArgs e)
        {
            this.DataTable = this.DataSet.Reports;
            this.DataTableTitle = "Отчет";
            this.TableSaveFileDialog.ShowDialog();
        }

        private void ActivitiesExportToolStripButton_Click(object sender, EventArgs e)
        {
            this.ChartSaveFileDialog.ShowDialog();
        }

        private void TableSaveFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            Program.ExportToExel(this.TableSaveFileDialog.FileName, this.DataTable, this.DataTableTitle);
        }

        private void ChartSaveFileDialog_FileOk(object sender, CancelEventArgs e)
        {
            this.ActivitiesChart.SaveImage(this.ChartSaveFileDialog.OpenFile(), ChartImageFormat.Png);
        }

        private void ActivitiesRowChangedHeandler(object sender, DataSet.ActivitiesRowChangeEvent e)
        {
            this.ActivitiesChart.Annotations.Clear();
            this.ActivitiesChart.DataBind();
        }
    }
}
